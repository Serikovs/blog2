﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using BlogCamp.Data;
using BlogCamp.Models;
using Microsoft.AspNetCore.Identity;

namespace BlogCamp.Services
{
    public class ServiceLayer : IServiceLayer
    {
        private BlogContext blogContext;
        

        public ServiceLayer(BlogContext blogContext)
        {
            this.blogContext = blogContext;
        }

        public bool AddArticle(string username, string caption, string articleText)
        {
            if (string.IsNullOrEmpty(caption))
            {
                return false;
            }

            var article = new Article
            {
                Username = username,
                Date = DateTime.Now,
                Caption = caption,
                ArticleText = articleText,
                Image = ""
            };
            

            blogContext.Articles.Add(article);
            blogContext.SaveChanges();

            return true;
        }
        
        public bool AddComment(string username, int articleID, string commentText )
        {
            var comment = new Comment
            {
                Username = username,
                Date = DateTime.Now,
                ArticleID = articleID,
                CommentText = commentText
            };

            blogContext.Comments.Add(comment);
            blogContext.SaveChanges();

            return true;
        }

        public bool AddVote(string username, int articleID, bool like)
        {
            var votes = 
                blogContext.Votes.Where(v => v.Username == username && v.ArticleID == articleID).ToList();

            if (votes.Count == 0)
            {
                var vote = new Vote()
                {
                    ArticleID = articleID,
                    Username = username,
                    LikeDislike = like
                };
                blogContext.Votes.Add(vote);
                blogContext.SaveChanges();
            }
            else
            {
                var vote = votes.FirstOrDefault();
                if (vote != null)
                {
                    vote.LikeDislike = like;
                }
                blogContext.SaveChanges();
            }

            return true;
        }

        public int GetArticlePositiveVotes(int articleID)
        {
            var votes = blogContext.Votes.Where(v => v.ArticleID == articleID && v.LikeDislike == true)
                .ToList();
            return votes.Count;
        }

        public int GetArticleNegativeVotes(int articleID)
        {
            var votes = blogContext.Votes.Where(v => v.ArticleID == articleID && v.LikeDislike == false)
                .ToList();
            return votes.Count;
        }

        public List<Comment> GetArticleComments(int articleID)
        {
            var result = blogContext.Comments
                .Where(a => a.ArticleID == articleID)
                .OrderByDescending(d => d.Date)
                .ToList();

            return result;
        }

        public List<Article> GetHomePageArticles()
        {
            var result = new List<Article>();
            result.AddRange(GetLatestArticles());
            result.AddRange(GetMostVotedArticles());
            return result;
        }

        public List<Article> GetLatestArticles()
        {
            var result = blogContext.Articles
                .OrderByDescending(d => d.Date)
                .Take(5)
                .ToList();
            return result;
        }

        public List<Article> GetMostVotedArticles()
        {
            var votes = new Dictionary<int, int>();
            foreach (var article in blogContext.Articles)
            {
                var id = article.ID;
                votes.Add(id, GetArticlePositiveVotes(id));
            }

            var result = blogContext.Articles
                .OrderBy(article => votes[article.ID])
                .Take(3)
                .ToList();
            return result;
        }
    }
}
