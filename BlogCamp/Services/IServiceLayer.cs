﻿using BlogCamp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogCamp.Services
{
    public interface IServiceLayer
    {
        bool AddArticle(string username, string caption, string articleText);
        bool AddComment(string username, int articleID, string commentText);
        bool AddVote(string username, int articleID, bool like);
        int GetArticlePositiveVotes(int articleID);
        int GetArticleNegativeVotes(int articleID);

        List<Article> GetHomePageArticles();
        List<Article> GetLatestArticles();
        List<Article> GetMostVotedArticles();
        List<Comment> GetArticleComments(int articleID);
    }
}