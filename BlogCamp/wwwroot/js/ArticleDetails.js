﻿function AddComment(CommentText) {
    $.ajax({
        url: "/Comments/Add",
        data: { articleID: ArticleID, commentText: CommentText },
    });
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

document.getElementById("addComment").addEventListener("click", function (event) {
    var commentText = document.getElementById("comment").value;
    AddComment(commentText);
    document.getElementById("comment").value = "";
    sleep(1000);
    location.reload();
});

$('#myModal').on('shown.bs.modal', function () {
    $('#myInput').trigger('focus')
})