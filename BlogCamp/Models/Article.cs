﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogCamp.Models
{
    public class Article
    {
        public int ID { get; set; }
        public string Username { get; set; }
        public DateTime Date { get; set; }
        public string Caption { get; set; }
        public string ArticleText { get; set; }
        public string Image { get; set; }
       
    }
}
