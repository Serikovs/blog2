﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogCamp.Models
{
    public class Comment
    {
        public int ID { get; set; }
        public string Username { get; set; }
        public DateTime Date { get; set; }
        public int ArticleID { get; set; }
        public string CommentText { get; set; }
    }
}
