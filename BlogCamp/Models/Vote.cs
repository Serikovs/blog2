﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogCamp.Models
{
    public class Vote
    {
        public int ID { get; set; }
        public int ArticleID { get; set; }
        public string Username { get; set; }
        public bool LikeDislike { get; set; }
    }
}
