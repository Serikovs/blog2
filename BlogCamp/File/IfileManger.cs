﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;


namespace BlogCamp.Services
{
    public interface IFileManager
    {
        FileStream LoadImage(string imageFilename);
    }
}