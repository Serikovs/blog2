﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BlogCamp.Services
{
    public class FileManager : IFileManager
    {
        public FileStream LoadImage(string imageFilename)
        {
            return new FileStream("wwwroot\\images\\" + imageFilename, FileMode.Open, FileAccess.Read);
        }
    }
}