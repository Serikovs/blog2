﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlogCamp.Data;
using BlogCamp.Models;
using BlogCamp.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace BlogCamp.Controllers
{
    public class CommentsController : Controller 
    {
        private IServiceLayer _serviceLayer;
        private readonly BlogContext _context;
        
        public CommentsController(IServiceLayer serviceLayer, BlogContext context)
        {
            _serviceLayer = serviceLayer;
           _context = context;
        }
        public async Task<IActionResult> Index(int articleid)
        {
            return RedirectToAction("Details", "Articles", new { @id = articleid });
        }
        [Authorize]
        public async Task Add(int articleID, string commentText)
        {
            var username = User.Identity.Name;
            _serviceLayer.AddComment(username, articleID, commentText);
        }
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var comment = await _context.Articles.FindAsync(id);
            if (comment == null)
            {
                return NotFound();
            }
            return View(comment);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, int articleid, [Bind("Id,CommentText")] Comment comment)
        {
            if (id != comment.ID)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(comment);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CommentsExists(comment.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
           return RedirectToAction("Details", "Articles", new { @id = articleid });

        }
        public async Task<IActionResult> Delete(int id, int articleid)
        {
            var comments = await _context.Comments.FindAsync(id);
            _context.Comments.Remove(comments);
            await _context.SaveChangesAsync();
            return RedirectToAction("Details", "Articles",new {@id=articleid });
        }
        private bool CommentsExists(int id)
        {
            return _context.Comments.Any(e => e.ID == id);
        }
    }
}