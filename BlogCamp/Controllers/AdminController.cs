﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using BlogCamp.Models;
using Microsoft.AspNetCore.Authorization;
using BlogCamp.Services;
using BlogCamp.Data;

namespace BlogCamp.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private IServiceLayer serviceLayer;
        private readonly BlogContext context;
        RoleManager<IdentityRole> _roleManager;
        UserManager<BlogUser> _userManager;

        public AdminController(IServiceLayer serviceLayer, BlogContext context, RoleManager<IdentityRole> roleManager, UserManager<BlogUser> userManager)
        {
            this.serviceLayer = serviceLayer;
            this.context = context;
            _roleManager = roleManager;
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            ViewBag.Users = context.Users.ToList();
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Delete(string id)
        {
            BlogUser user = await _userManager.FindByIdAsync(id);

            if (user != null)
            {
                IdentityResult result = await _userManager.DeleteAsync(user);
                if (result.Succeeded)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return View("Error", result.Errors);
                }
            }
            else
            {
                return View("Error", new string[] { "User is not found" });
            }
        }

    }
}