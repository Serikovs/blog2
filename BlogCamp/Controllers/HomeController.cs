﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BlogCamp.Models;
using BlogCamp.Services;
using BlogCamp.Data;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace BlogCamp.Controllers
{
    public class HomeController : Controller
    {
        private IServiceLayer _serviceLayer;
        private readonly BlogContext _context;
        private IFileManager _fileManager;
        

        public HomeController(IServiceLayer serviceLayer, BlogContext context, IFileManager fileManager)
        {
            _serviceLayer = serviceLayer;
            _context = context;
            _fileManager = fileManager;
        }

        //[Authorize(Roles = "admin, user")]
        public IActionResult Index()
        {
            ViewBag.Latest = _serviceLayer.GetLatestArticles();
            ViewBag.MostVoted = _serviceLayer.GetMostVotedArticles();
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public IActionResult Login()
        {
          
            return Redirect("~/Identity/Account/Login");
        }

        [HttpGet("image/{image}")]
        public IActionResult Image(string image)
        {
           var mime = image.Substring(image.LastIndexOf('.') + 1);
           return new FileStreamResult(_fileManager.LoadImage(image), $"images/{mime}");
        }
    }
}
